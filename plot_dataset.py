import torch
import numpy as np
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
from torch.utils.data import DataLoader, SubsetRandomSampler

# Configuration
samples_per_class = 2
num_workers = 4  # Adjust based on your machine's capabilities

def plot_image_and_channels(image, class_name):
    """
    Plot the original image and its R, G, B channels.
    """
    fig, axs = plt.subplots(1, 4, figsize=(10, 2.5))  # 1 row, 4 columns
    axs[0].imshow(np.transpose(image.numpy(), (1, 2, 0)))
    axs[0].set_title(f'Original\n{class_name}')
    
    for i, color in enumerate(['Red', 'Green', 'Blue']):
        # Create an image for the channel
        channel_image = torch.zeros_like(image)
        channel_image[i] = image[i]
        axs[i+1].imshow(np.transpose(channel_image.numpy(), (1, 2, 0)))
        axs[i+1].set_title(f'{color} channel')
    
    for ax in axs:
        ax.axis('off')
    
    plt.tight_layout()
    plt.show()

def sample_and_plot_efficient(dataset, samples_per_class, num_workers):
    """
    Efficiently sample images from each class and plot them with their RGB channels.
    """
    class_samples = {}
    for idx, (path, class_index) in enumerate(dataset.imgs):
        class_name = dataset.classes[class_index]
        if class_name not in class_samples:
            class_samples[class_name] = []
        class_samples[class_name].append(idx)
    
    for class_name, indices in class_samples.items():
        # Ensure not to sample more than what's available
        num_samples = min(samples_per_class, len(indices))
        sampled_indices = np.random.choice(indices, num_samples, replace=False)
        
        # Create a DataLoader for the sampled indices
        sampler = SubsetRandomSampler(sampled_indices)
        loader = DataLoader(dataset, batch_size=1, sampler=sampler, num_workers=num_workers)
        
        for image, label in loader:
            image = torch.squeeze(image)
            plot_image_and_channels(image, class_name)

# Call the function
sample_and_plot_efficient(dataset, samples_per_class, num_workers)